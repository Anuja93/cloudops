#!/bin/sh

echo " fetch the value of subscription ID"
sub_id=$(az account show --query id --output tsv) 

echo $sub_id

# echo "network nsg rule create for Machine 1"
# az network nsg rule create --name /subscriptions/$sub_id/resourceGroups/myRGroup/providers/Microsoft.Compute/virtualMachines/Machine1
# az monitor metrics list --resource /subscriptions/c074cd56-a465-440c-ba7a-c9c6eb686f7b/resourceGroups/myRGroup/providers/Microsoft.Compute/virtualMachines/Machine1

echo "CPU Monitoring for Machine1"
az monitor metrics list --resource /subscriptions/$sub_id/resourceGroups/myRGroup/providers/Microsoft.Compute/virtualMachines/Machine1

echo "Network Monitoring for Machine1"
az monitor metrics list --resource  /subscriptions/$sub_id/resourceGroups/myRGroup/providers/Microsoft.Network/networkInterfaces/Machine1VMNic

echo "Disk Monitoring for Machine 1"
az monitor metrics list --resource /subscriptions/$sub_id/resourceGroups/myRGroup/providers/Microsoft.Compute/disks/machine1_disk1_5923d8f6d61844179e4e1d0cb771dfd5

# echo "network nsg rule create for machine 2"
# az network nsg rule create --name /subscriptions/$sub_id/resourceGroups/myRGroup/providers/Microsoft.Compute/virtualMachines/Machine2

echo "CPU Monitoring for Machine2"
az monitor metrics list --resource /subscriptions/$sub_id/resourceGroups/myRGroup/providers/Microsoft.Compute/virtualMachines/Machine2

echo "Network Monitoring for Machine2"
az monitor metrics list --resource  /subscriptions/$sub_id/resourceGroups/myRGroup/providers/Microsoft.Network/networkInterfaces/Machine2VMNic

echo "Disk Monitoring for Machine 2"
az monitor metrics list --resource /subscriptions/$sub_id/resourceGroups/myRGroup/providers/Microsoft.Compute/disks/Machine2_disk1_2787c5c599d246638796a52760bb15d8

