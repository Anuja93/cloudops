//Install Azure CLI
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

//Azure Login
az login

//Set Subscription ID
echo "Enter Subscription ID:"
read sub_id
az account set --subscription $sub_id

// Creating the resource group 
az group create --name myRGroup --location eastus

//creating the network for the resource group 
az network vnet create --name myVirtualNetwork --resource-group myRGroup --subnet-name default

// create the actual vm 1
az vm create --resource-group myRGroup --name machine1 --image UbuntuLTS --admin-username azureuser --generate-ssh-keys --no-wait

// create second machine 
az vm create --resource-group myRGroup --name Machine2 --image UbuntuLTS --admin-username azureuser --generate-ssh-keys

//creating the rule for inbound to provide the connectivity between two kms
az network nsg rule create --name icmp_rule --nsg-name Machine1NSG --priority 3001 --resource-group myRGroup --access Allow --direction Inbound --protocol Icmp --source-port-ranges '*' --destination-port-ranges '*'
az network nsg rule create --name icmp_rule --nsg-name Machine2NSG --priority 3001 --resource-group myRGroup --access Allow --direction Inbound --protocol Icmp --source-port-ranges '*' --destination-port-ranges '*'
